import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'News Links',
      theme: ThemeData(
        colorScheme: ColorScheme.highContrastLight(),
        primarySwatch: Colors.brown,
      ),
      home: MyHomePage(title: 'News Links'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final String title;

  MyHomePage({Key? key, required this.title}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final channels = [
    {
      'title': '24 News',
      'icon': 'resources/24news.jpg',
      'link': 'https://www.youtube.com/channel/UCup3etEdjyF1L3sRbU-rKLw/live'
    },
    {
      'title': 'Manorama News',
      'icon': 'resources/manorama.jpg',
      'link': 'https://www.youtube.com/channel/UCP0uG-mcMImgKnJz-VjJZmQ/live'
    },
    {
      'title': 'Asianet News',
      'icon': 'resources/asianet.jpg',
      'link': 'https://www.youtube.com/user/asianetnews/live'
    },
    {
      'title': 'Media One TV Live',
      'icon': 'resources/mediaone.jpg',
      'link': 'https://www.youtube.com/c/MediaoneTVLive/live'
    },
    {
      'title': 'Mathrubhumi News',
      'icon': 'resources/mathrubhumi.jpg',
      'link': 'https://www.youtube.com/mathrubhumitv/live'
    },
    {
      'title': 'News 18 - Kerala',
      'icon': 'resources/news18.jpg',
      'link': 'https://www.youtube.com/c/News18Kerala/live'
    },
    {
      'title': 'Reporter Live',
      'icon': 'resources/reporter.jpg',
      'link': 'https://www.youtube.com/c/reporterlive/live'
    },
    {
      'title': 'Mangalam Television',
      'icon': 'resources/mangalam.jpg',
      'link': 'https://www.youtube.com/c/mangalamtv/live'
    },
    {
      'title': 'Kairali News Live',
      'icon': 'resources/kairali.jpg',
      'link': 'https://www.youtube.com/c/KairaliNewsLive/live'
    },
    {
      'title': 'NDTV 24x7 English',
      'icon': 'resources/ndtv.jpg',
      'link': 'https://www.youtube.com/c/NDTV/live'
    },
    {
      'title': 'CNN-News18',
      'icon': 'resources/cnn.jpg',
      'link': 'https://www.youtube.com/c/cnnnews18/live'
    },
    {
      'title': 'India Today',
      'icon': 'resources/indiatoday.jpg',
      'link': 'https://www.youtube.com/c/indiatoday/live'
    }
  ];

  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: makeCards(),
      ),
    );
  }

  List<Card> makeCards() {
    return channels.map((e) => Card(
      child: Semantics(
        // label: e['title'] as String,
        hint: 'Tap to open',
        child: ListTile(
          contentPadding: EdgeInsets.all(7),
          title: Text(
            e['title'] as String,
            style: TextStyle(
              fontWeight: FontWeight.w900,
              fontSize: 20,
            ),
          ),
          leading: Image(
            image: AssetImage(
                e['icon'] as String
            ),
          ),
          onTap: () => launch(e['link'] as String),
        ),
      ),
    )).toList();
  }
}
